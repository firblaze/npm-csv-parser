import { type Config } from "@jest/types"
import { defaults } from "jest-config"
import merge from "webpack-merge"

const config = [defaults] as Config.InitialOptions[]

// ts-jest
config.push({
	preset: "ts-jest",
	transform: {
		"^.+\\.m?[tj]sx?$": "ts-jest",
	},
	transformIgnorePatterns: ["/node_modules/.*\\.js$"],
	moduleFileExtensions: ["js", "mjs", "cjs", "ts", "tsx"],
})

// resolve
config.push({
	collectCoverageFrom: ["./src/*.*"],
	coveragePathIgnorePatterns: ["/*backup*/"],
})

export default merge(config)
