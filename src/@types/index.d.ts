declare namespace CsvParserTranspose {

  interface exportFn {
    csvParser: CsvParser
    transposeMatrix: TransposeMatrix
  }

  type CsvParser = (inputCsv: string, options?: Options) => Promise<string[][]>
  type TransposeMatrix = <T>(input: T[][]) => {matrix: T[][], maxColumnSize: number}

  interface Options {
    csv?: {
      separator?: string
      newLine?: string
    },
    dist?: {
      lineTitle?: {
        separator?: string
        range?: number
      }
      columnTitle?: {
        separator?: string
        range?: number
      }
    },
    transpose?: boolean
    categoryFill?: boolean
  }

  interface OptionsMg {
    csv: {
      separator: string
      newLine: string
    },
    dist: {
      lineTitle: {
        separator: string
        range: number
      }
      columnTitle: {
        separator: string
        range: number
      }
    },
    transpose: boolean
    categoryFill: boolean
  }
}

declare const csvParserTranspose: CsvParserTranspose.exportFn
export = csvParserTranspose
