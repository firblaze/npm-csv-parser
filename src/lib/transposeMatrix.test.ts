import { transposeMatrix } from "./transposeMatrix"

describe("正常系", () => {
	it("str", async () => {
		expect(
			transposeMatrix([
				["a", "b", "c"],
				["111", "222", "333"],
			])
		).toEqual({
			matrix: [
				["a", "111"],
				["b", "222"],
				["c", "333"],
			],
			maxColumnSize: 3,
		})
	})
	it("num", async () => {
		expect(
			transposeMatrix([
				[1, 2, 3, 4],
				[11, 22, 33, 44],
				[111, 222, 333, 444],
			])
		).toEqual({
			matrix: [
				[1, 11, 111],
				[2, 22, 222],
				[3, 33, 333],
				[4, 44, 444],
			],
			maxColumnSize: 4,
		})
	})
})
