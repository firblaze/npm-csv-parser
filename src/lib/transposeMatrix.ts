import fn from "../@types"

export const transposeMatrix: typeof fn.transposeMatrix = (matrix) => {
	let maxColumnSize = 0
	for (const iterator of matrix) {
		maxColumnSize = Math.max(iterator.length, maxColumnSize)
	}

	/** @type {typeof matrix} */
	const trMtx = new Array(maxColumnSize)
	console.log({ trMtx, maxColumnSize })
	let trLineKey = 0
	for (const trMtxLineDummy of trMtx) {
		trMtx[trLineKey] = new Array(matrix.length)
		let trColumnKey = 0
		for (const trMtxValDummy of trMtx[trLineKey]) {
			trMtx[trLineKey][trColumnKey] = matrix[trColumnKey][trLineKey]
			// console.log("l2", { trMtx, trColumnKey })
			trColumnKey += 1
		}
		// console.log("l1", { trMtx, trLineKey })
		trLineKey += 1
	}

	return { matrix: trMtx, maxColumnSize: maxColumnSize }
}
