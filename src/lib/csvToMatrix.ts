export const csvToMatrix = (csvStr: string, newLine: string, separator: string) => {
	const csvMatrix: string[][] = []
	const csvLineArr = csvStr.split(newLine)
	for (const csvlineStr of csvLineArr) {
		if (!csvlineStr) continue
		const valArr = csvlineStr.trimEnd().split(separator)
		csvMatrix.push(valArr)
	}
	console.log({ csvMatrix })
	return csvMatrix
}
