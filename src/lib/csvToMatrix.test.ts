import { describe, expect, it } from "@jest/globals"
import { csvToMatrix } from "./csvToMatrix"

describe("正常系", () => {
	it("default", async () => {
		expect(
			csvToMatrix(
				`
a	b	c
111	222	333
`,
				"\n",
				"\t"
			)
		).toEqual([
			["a", "b", "c"],
			["111", "222", "333"],
		])
	})
})
