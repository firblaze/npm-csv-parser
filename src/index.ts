import fn from "./@types"
import { csvToMatrix } from "./lib/csvToMatrix"
import { transposeMatrix as transposeMatrixLc } from "./lib/transposeMatrix"

export const csvParser: typeof fn.csvParser = async (csvStr, opt = {}) => {
	const mgOpt = {
		csv: {
			separator: opt?.csv?.separator || "\t",
			newLine: opt?.csv?.newLine || "\n",
		},
		dist: {
			lineTitle: {
				range: opt?.dist?.lineTitle?.range || 0,
				separator: opt?.dist?.lineTitle?.separator || "/"
			},
			columnTitle: {
				range: opt?.dist?.columnTitle?.range || 0,
				separator: opt?.dist?.columnTitle?.separator || "/"
			}
		},
		transpose: opt?.transpose || false,
		categoryFill: opt?.categoryFill || false
	}
	console.log({mgOpt})

	const csvMatrix = csvToMatrix(csvStr, mgOpt.csv.newLine, mgOpt.csv.separator)

	if (mgOpt.transpose) {
		return transposeMatrixLc(csvMatrix).matrix
	} else {
		return csvMatrix
	}
}

export const transposeMatrix = transposeMatrixLc
