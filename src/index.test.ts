import { describe, expect, it } from "@jest/globals"
import { csvParser } from "./index"

describe("正常系", () => {
	const inputCsv = [
		`
a	b	c	
111	222	333

`,
		`	`,
	]

	it("output matrix", async () => {
		expect(await csvParser(inputCsv[0])).toEqual([
			["a", "b", "c"],
			["111", "222", "333"],
		])
		expect(await csvParser(inputCsv[0], { transpose: true })).toEqual([
			["a", "111"],
			["b", "222"],
			["c", "333"],
		])
		expect(await csvParser(inputCsv[1])).toEqual([[""]])
	})
})
