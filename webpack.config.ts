import path from "path"
import { type Configuration } from "webpack"
import merge from "webpack-merge"

const cfg = [] as Configuration[]

// dev <-> prod
cfg.push({
  mode: "production"
})

// in
cfg.push({
	entry: {
		index: path.resolve(__dirname, "./src/index.ts"),
	},
  resolve: {
    extensions: [".ts",".js",".mjs",".cjs"]
  }
})

// out
cfg.push({
	output: {
		path: path.resolve(__dirname, "dist"),
		filename: "[name].cjs",
	},
})

// ts -> cjs
cfg.push({
	module: {
		rules: [
			{
				test: /\.m?[tj]s$/,
				loader: "ts-loader",
			},
		],
	},
})

export default merge(cfg)
