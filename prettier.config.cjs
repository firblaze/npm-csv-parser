// @ts-cehck
/** @type {import("prettier").Config} */
const prettierCfg = {
	trailingComma: "es5",
	semi: false,
	useTabs: true,
	tabWidth: 2,
}
module.exports = prettierCfg
